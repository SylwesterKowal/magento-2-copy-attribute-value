<?php
declare(strict_types=1);

namespace Kowal\CopyAttribute\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Copy extends Command
{

    const NAME_SOURCE = "source";
    const NAME_TARGET = "target";
    const NAME_TABLE = "tabela";
    const SKU_FILTER = "sku_filter";
    const NAME_STORID = "store";
    const MODE = "production";
    const SKIP_EMPTY = "skip";

    protected $storeid = 0;
    protected $collection = 0;
    protected $production = false;
    protected $skip = false;
    protected $source = null;
    protected $target = null;
    protected $table = null;
    protected $magentoService = null;

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurable,
        \Kowal\CopyAttribute\lib\MagentoService $magentoService
    )
    {
        parent::__construct();
        $this->collectionFactory = $collectionFactory;
        $this->magentoService = $magentoService;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    )
    {
        $this->source = $input->getArgument(self::NAME_SOURCE);
        $this->target = $input->getArgument(self::NAME_TARGET);
        $this->table = $input->getArgument(self::NAME_TABLE);
        $this->sku_filter = $input->getArgument(self::SKU_FILTER);
        $this->storid = ($input->getArgument(self::NAME_STORID)) ? $input->getArgument(self::NAME_STORID) : 0;
        $this->production = $input->getOption(self::MODE);
        $this->skip = $input->getOption(self::SKIP_EMPTY);

        $this->magentoService->setStoreID($this->storeid);

        if (!empty($this->source) && !empty($this->target) && !empty( $this->table)) {
            $output->writeln("START");
            if ($this->production) {
                $output->writeln("Produkcja z " . $this->source . " do " . $this->target . " storeId: " . $this->storid);
            } else {
                $output->writeln("TEST z " . $this->source . " do " . $this->target . " storeId: " . $this->storid);
            }
            $this->getCollection()
                ->readCollection();

                $output->writeln("KONIEC");
        } else {
            $output->writeln("Proszę podać kod źródłowy atrybutu oraz kod docelowy atrybutu");
            $output->writeln("kowal_copyattribute:copy [options] [--] [<source> [<target> <table> [<store>]]]");
        }
    }

    private function getCollection()
    {
        $this->collection = $this->collectionFactory->create()
            ->addStoreFilter($this->storeid)
            ->addAttributeToSelect('*');

        if(!empty($this->sku_filter)) $this->collection->addFieldToFilter('like', ['sku' => '%'.$this->sku_filter.'%']);
        return $this;
    }


    private function readCollection()
    {
        $source = "get" . str_replace(" ", "", ucwords(str_replace("_", " ", $this->source)));
        foreach ($this->collection as $product) {

            if ($product->getTypeId() == 'configurable') {
                $child_products = $product->getTypeInstance()->getUsedProducts($product, null);
                if (count($child_products) > 0) {
                    foreach ($child_products as $child) {
                        $source_value = $this->getValue($child->getSku());

                        if ($this->skip && empty($source_value)) continue;

                        echo $child->getSku() . ": " . $source_value . "\n";
                        if($this->target != "sku"){
                            $this->magentoService->updateAtt($child->getSku(), $source_value, $this->target,  $this->table);
                        }else{
                            $this->magentoService->replaceSku($child->getSku(), $source_value, $this->target,  $this->table);
                        }

                    }
                }
            } else {
                $source_value = $this->getValue($product->getSku());
                if ($this->skip && empty($source_value) ) continue;

                echo $product->getSku() . ": " . $source_value . "\n";
                if($this->target != "sku") {
                    $this->magentoService->updateAtt($product->getSku(), $source_value, $this->target, $this->table);
                }else{
                    $this->magentoService->replaceSku($product->getSku(), $source_value, $this->target,  $this->table);
                }
            }

            if (!$this->production) {
                break;
            }
        }
    }

    protected function getValue($_sku)
    {
        $sku = ($_sku) ? " AND catalog_product_entity.sku = '{$_sku}'" : "";
        $query = "
        SELECT nametable.value AS c2c_ship
        FROM `{$this->table}` AS nametable
                 LEFT JOIN catalog_product_entity
                           ON nametable.entity_id = catalog_product_entity.entity_id
        
        WHERE nametable.attribute_id = (SELECT attribute_id
                                        FROM `eav_attribute`
                                        WHERE `entity_type_id` = 4
                                          AND `attribute_code` LIKE '{$this->source}' )
        AND nametable.store_id = {$this->storeid} {$sku}";
        $connection = $this->magentoService->_getReadConnection();

        return $connection->fetchOne($query);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_copyattribute:copy");
        $this->setDescription("Kopiowanie atrybutu");
        $this->setDefinition([
            new InputArgument(self::NAME_SOURCE, InputArgument::OPTIONAL, "Kod atrybutu źródłowego z którego będą kopiowane dane"),
            new InputArgument(self::NAME_TARGET, InputArgument::OPTIONAL, "Kod atrybutu docelowego do którego będą zapisywane dane"),
            new InputArgument(self::NAME_TABLE, InputArgument::OPTIONAL, "Nazwa tabeli: catalog_product_entity_decimal, catalog_product_entity_int, catalog_product_entity_varchar, catalog_product_entity_text"),
            new InputArgument(self::SKU_FILTER, InputArgument::OPTIONAL, "Filt LIKE na sku"),
            new InputArgument(self::NAME_STORID, InputArgument::OPTIONAL, "Store ID - domyślnie 0"),
            new InputOption(self::MODE, "-p", InputOption::VALUE_NONE, "Tryb produkcyjny"),
            new InputOption(self::SKIP_EMPTY, "-s", InputOption::VALUE_NONE, "Pomiń puste źródło")
        ]);
        parent::configure();
    }
}

