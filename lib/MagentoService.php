<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 2019-06-13
 * Time: 21:50
 */

namespace Kowal\CopyAttribute\lib;


class MagentoService
{
    public $store_id = 0;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resourceConnection;

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection
    )
    {
        $this->resourceConnection = $resourceConnection;
    }

    public function setStoreID($storeId)
    {
        $this->store_id = $storeId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function _getReadConnection()
    {
        return $this->_getConnection('core_read');
    }

    /**
     * @return mixed
     */
    public function _getWriteConnection()
    {
        return $this->_getConnection('core_write');
    }

    /**
     * @param string $type
     * @return mixed
     */
    private function _getConnection($type = 'core_read')
    {
        return $this->resourceConnection->getConnection($type);
    }

    /**
     * @param $tableName
     * @return mixed
     */
    private function _getTableName($tableName)
    {
        return $this->resourceConnection->getTableName($tableName);
    }

    /**
     * @param $attributeCode
     * @return mixed
     */
    private function _getAttributeId($attributeCode)
    {
        $connection = $this->_getReadConnection();
        $sql = "SELECT attribute_id FROM " . $this->_getTableName('eav_attribute') . " WHERE entity_type_id = ? AND attribute_code = ?";
        return $connection->fetchOne(
            $sql,
            [
                $this->_getEntityTypeId('catalog_product'),
                $attributeCode
            ]
        );
    }

    /**
     * @param $entityTypeCode
     * @return mixed
     */
    private function _getEntityTypeId($entityTypeCode)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT entity_type_id FROM " . $this->_getTableName('eav_entity_type') . " WHERE entity_type_code = ?";
        return $connection->fetchOne(
            $sql,
            [
                $entityTypeCode
            ]
        );
    }

    /**
     * @param $sku
     * @return mixed
     */
    private function _getIdFromSku($sku)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT entity_id FROM " . $this->_getTableName('catalog_product_entity') . " WHERE sku = ?";
        return $connection->fetchOne(
            $sql,
            [
                $sku
            ]
        );
    }

    /**
     * @param $sku
     * @return mixed
     */
    public function checkIfSkuExists($sku)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT COUNT(*) AS count_no FROM " . $this->_getTableName('catalog_product_entity') . " WHERE sku = ?";
        return $connection->fetchOne($sql, [$sku]);
    }

    /**
     * @param $sku
     * @param $value
     * @param $attr_code
     * @param $table_name
     */
    public function updateAtt($sku, $value, $attr_code, $table_name)
    {
        $connection = $this->_getWriteConnection();
        $entityId = $this->_getIdFromSku($sku);
        $attributeId = $this->_getAttributeId($attr_code);
        $sql = "INSERT INTO " . $this->_getTableName($table_name) . " (attribute_id, store_id, entity_id, value) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE value=VALUES(value)";
//        $connection->query(
//            $sql,
//            [
//                $attributeId,
//                $this->store_id,
//                $entityId,
//                $value
//            ]
//        );
    }

    /**
     * Zmiana SKU
     * @param $sku
     * @param $value
     * @param $attr_code
     * @param $table_name
     */
    public function replaceSku($sku, $value, $attr_code, $table_name= "catalog_product_entity")
    {
        $connection = $this->_getWriteConnection();
        $entityId = $this->_getIdFromSku($sku);
        $attributeId = $this->_getAttributeId($attr_code);
        $sql = "UPDATE `" . $this->_getTableName($table_name) . "` SET `sku` = ? WHERE `" . $this->_getTableName($table_name) . "`.`entity_id` = ?;";
//        $connection->query(
//            $sql,
//            [
//                $value,
//                $entityId
//            ]
//        );
    }

}